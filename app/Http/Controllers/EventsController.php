<?php

namespace App\Http\Controllers;

use App\Models\Events;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Events::orderBy('id', 'DESC')->get();
        return view('dashboard', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'type' => 'required|max:255',
            'start_date' => 'required|before:end_date',
            'end_date' => 'required|after:start_date',
            'status' => 'required'
        ]);

        $event = new Events;
        $event->name = $request->name;
        $event->type = $request->type;
        $event->start_date = $request->start_date;
        $event->end_date = $request->end_date;
        $event->status = $request->status;
        $event->save();
        return redirect('dashboard')->with('event_msg', 'Event has been created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function editEvent($id)
    {
        $event = Events::find($id);
        return view('event.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function updateEvent(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'type' => 'required|max:255',
            'start_date' => 'required|before:end_date',
            'end_date' => 'required|after:start_date',
            'status' => 'required'
        ]);

        $event = Events::find($request->id);
        $event->name = $request->name;
        $event->type = $request->type;
        $event->start_date = $request->start_date;
        $event->end_date = $request->end_date;
        $event->status = $request->status;
        $event->save();
        return redirect('dashboard')->with('event_msg', 'Event has been updated successfully.');
    }

    /**
     * Delete the event from the id.
     *
     * @param  $id
     * @return return to previous page with message
     */
    public function deleteEvent($id)
    {
        Events::where('id', $id)->delete();
        return response()->json(["status" => '200', "code" => 'Event has been deleted successfully']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $events = Events::all();
        return response()->json($events);
    }
}
