<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>

<body class="font-sans antialiased">
    <div class="min-h-screen bg-gray-100">
        @include('layouts.navigation')

        <!-- Page Heading -->
        <header class="bg-white shadow">
            <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                {{ $header }}
            </div>
        </header>

        <!-- Page Content -->
        <main>
            {{ $slot }}
        </main>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $('#event_table').DataTable({
                ajax: {
                    url: "{{ route('event.show') }}",
                    dataSrc: ""
                },
                "columns": [{
                        "data": "name"
                    },
                    {
                        "data": "type"
                    },
                    {
                        "data": "start_date"
                    },
                    {
                        "data": "end_date"
                    },
                    {
                        "data": "status"
                    },
                    {
                        "data": "action",
                        "render": function(data, type, row, meta) {
                            return '<a onclick="deleteUser(' + row.id + ')" class="btn btn-danger text-white mr-2"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a><a href="/edit/' + row.id + '" class="btn btn-success"><i class="fa fa-edit"></i> Edit</a>';
                        }
                    }
                ]
            });
        });

        function deleteUser(id) {
            swal({
                title: "Are you sure?",
                text: "You want to Delete.",
                icon: 'warning',
                dangerMode: true,
                buttons: true
            }).then(function(willDelete) {
                if (willDelete) {
                    $.ajax({
                        url: '/delete/' + id,
                        type: "get"
                    }).done(function(data) {
                        if (data.status == "200") {
                            swal(data.status, {
                                title: data.code
                            });
                        }
                        var table = $('#event_table').DataTable();
                        table.ajax.reload();
                        swal("Deleted successfully.!", {
                            icon: "success",
                        });
                    }).fail(function(jqXHR, ajaxOptions, thrownError) {
                        swal("Something wrong.", {
                            title: 'Cancelled',
                            icon: "error",
                        });
                    });
                } else {
                    swal("Record is safe", {
                        title: 'Cancelled',
                        icon: "error",
                    });
                }
            });
        }
    </script>
</body>

</html>