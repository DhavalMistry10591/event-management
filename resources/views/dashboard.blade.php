<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Event Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if(Session::has('event_msg'))
            <div class="alert alert-success" role="alert">
                <i class="fa fa-check-circle"></i> {{Session::get('event_msg')}}
            </div>
            @endif
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="{{ route('event.create') }}" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add event</a>
                        </div>
                    </div> <br />

                    <table id="event_table" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Event Type</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Event Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>