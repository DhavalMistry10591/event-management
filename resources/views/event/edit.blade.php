<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Update Event
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-3xl mx-auto sm:px-6 lg:px-6">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="{{ route('event.update') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{$event->id}}"/>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <strong>Name:</strong>
                                    <input type="text" name="name" class="form-control" value="{{$event->name}}" placeholder="Enter Name">
                                    @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <strong>Event Type:</strong>
                                    <input type="text" name="type" class="form-control" value="{{$event->type}}" placeholder="Enter Event Type">
                                    @if ($errors->has('type'))
                                    <span class="text-danger">{{ $errors->first('type') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <strong>Start Date:</strong>
                                    <input type="date" name="start_date" class="form-control" value="{{$event->start_date}}" placeholder="Select Start Date">
                                    @if ($errors->has('start_date'))
                                    <span class="text-danger">{{ $errors->first('start_date') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <strong>End Date:</strong>
                                    <input type="date" name="end_date" class="form-control" value="{{$event->end_date}}" placeholder="Select End Date">
                                    @if ($errors->has('end_date'))
                                    <span class="text-danger">{{ $errors->first('end_date') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <strong>Status:</strong>
                                    <select class="form-control" id="status" name="status" value="{{$event->status}}">
                                        <option value="">-------- Select ----------</option>
                                        <option value="confirmed" {{ ($event->status == "confirmed" ? "selected":"") }}>Confirmed</option>
                                        <option value="pending" {{ ($event->status == "pending" ? "selected":"") }}>Pending</option>
                                    </select>
                                    @if ($errors->has('status'))
                                    <span class="text-danger">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>



</x-app-layout>