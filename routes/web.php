<?php

use App\Http\Controllers\EventsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::get('dashboard', [EventsController::class, 'index'])->middleware(['auth'])->name('dashboard');
Route::get('create', [EventsController::class, 'create'])->name('event.create');
Route::post('store', [EventsController::class, 'store'])->name('event.store');
Route::get('delete/{id}', [EventsController::class, 'deleteEvent'])->name('event.delete');
Route::get('edit/{id}', [EventsController::class, 'editEvent']);
Route::post('update', [EventsController::class, 'updateEvent'])->name('event.update');
Route::get('events', [EventsController::class, 'show'])->name('event.show');

require __DIR__.'/auth.php';
